# README #

The script "wemo_control.sh" can be used to control a WeMo switch etc via shell.

**/path/to/wemo_control.sh wemoIP Command**

E.g. if this is located in the location "Applications" --> "WeMo" and your WeMo switch has the local IP address of 192.168.0.123 you can enter:

* /Applications/WeMo/wemo_control.sh 192.168.0.123 ON
* /Applications/WeMo/wemo_control.sh 192.168.0.123 OFF
* /Applications/WeMo/wemo_control.sh 192.168.0.123 TOGGLE
* /Applications/WeMo/wemo_control.sh 192.168.0.123 ONTIME
* /Applications/WeMo/wemo_control.sh 192.168.0.123 GETSTATE
* /Applications/WeMo/wemo_control.sh 192.168.0.123 GETSIGNALSTRENGTH
* /Applications/WeMo/wemo_control.sh 192.168.0.123 GETFRIENDLYNAME

On a Mac you can also turn this into an application very easily just open "Script Editor" (Applications/Utilities or use spotlight search) 

Simply enter:
**do shell script "sh /Applications/WeMo/wemo_control.sh 192.168.0.123 TOGGLE"**

Make sure your path to "wemo_control.sh", "IP address" (192.168.0.123) and the Command (TOGGLE) is correct and save this as an application.